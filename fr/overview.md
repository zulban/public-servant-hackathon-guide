[(English)](../en/overview.md)

# Qu'est-ce qu'un hackathon?

Un [hackathon](https://fr.wikipedia.org/wiki/Hackathon) est un événement semblable à un sprint de conception dans lequel des programmeurs informatiques, des concepteurs graphiques, des concepteurs d'interfaces, des gestionnaires de projet et autres, souvent avec des experts de domaine, collaborent.

Un hackathon n'est pas une conférence ou une convention. Presque tout le temps du calendrier est vide car les gens ont besoin de temps pour construire des choses intéressantes.

Les prix dans les hackathons sont excellents car beaucoup de personnes voudront se joindre à notre événement et déployer beaucoup d'efforts. Toutefois, les prix incitent également à ne pas assister à nos présentations en direct et à nos tables rondes.

Presque tous les projets créés dans un hackthon sont mauvais. Beaucoup sont abandonnés et jamais soumis. La plupart des soumissions sont mauvaises. C'est bon. Les hackathons concernent l'expérimentation, l'apprentissage et la réalisation de prototypes rapides.

Les hackathons ont des mentors et des experts (parfois avec des tables/stands). Il doit y avoir beaucoup de mentors présents lors d'un hackathon, prêts à aider.

# Jalons

Voir [jalons](internal/milestones.md) pour une vue chronologique de haut niveau des tâches les plus prioritaires.

# Interne versus Externe

Des hackathons gouvernementaux peuvent être organisés:

* Entièrement par les fonctionnaires
* Entièrement par le secteur privé
* En partie par public, en partie privé

Dès le début, vous devrez décider quels [rôles](roles) seront gérés en interne et en externe.

Par exemple, vous pouvez souhaiter que le coordinateur marketing et le maître de cérémonie soient confiés au secteur public, mais tous les autres rôles relèvent du contrat avec le secteur privé.

# Les rôles

Il est essentiel que tout le monde connaisse son rôle et en soit heureux. Les personnes peuvent être affectées à plusieurs rôles, mais il est recommandé que chaque rôle ne comporte qu'une seule personne. Sinon, si tout le monde est responsable de tout, personne ne se sent responsable de rien.

Les rôles facilitent également la réaffectation de personnes si nécessaire.

* [Coordinateur bilinguisme](roles/bilinguisme.md)
* [Gardien](roles/custodian.md)
* [Coordinateur des juges](roles/juges.md)
* [Coordinateur marketing](roles/marketing.md)
* [Maître de cérémonie](roles/mc.md)
* [Coordinateur des mentors](roles/mentors.md)
* [Membre du comité organisateur](roles/committee.md)
* [Coordinateur des présentations](roles/presentations.md)
* [Coordinateur du prix](roles/prizes.md)
* [Coordinateur d'inscription](roles/registration.md)
* [Coordinateur technologique](roles/tech.md)
* [Coordinateur de la formation](roles/training.md)
* [Coordinateur de site](roles/venue.md)
* [Coordinateur web](roles/web.md)

# Exemples

Vous pouvez trouver plusieurs exemples de documents réels [ici](exemples).

# Des principes

Voici quelques principes importants du gouvernement du Canada pouvant régir votre hackathon:

* [Open as a Foundation for Digital Government](https://open.canada.ca/en/blog/open-foundation-digital-government) qui recommande des formats ouverts pour toutes les diapositives, documents et données.
* [L'abc des données ouvertes](https://ouvert.canada.ca/fr/principes-de-donnees-ouvertes) qui introduit et explique les données ouvertes.
* [Directive du SCT sur la gestion de la technologie de l'information](https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=15249) qui demande aux fonctionnaires de commencer par examiner les solutions logiciel libre et ouvert, puis de les publier code écrit par les fonctionnaires comme logiciel libre par défaut.

# Voir également

Si vous envisagez sérieusement de planifier un hackathon, c'est une bonne idée de lire toutes ces ressources.

* [hackathon.guide](https://hackathon.guide/)
* [Liste de contrôle pour organiser un marathon de programmation](http://www.agr.gc.ca/fra/a-propos-de-nous/planification-et-rapports/resultats-du-renouvellement-de-la-fonction-publique-d-agriculture-et-agroalimentaire-canada/marathon-de-programmation-sur-les-donnees/liste-de-controle-pour-organiser-un-marathon-de-programmation/?id=1542987317998) de Agriculture et Agroalimentaire Canada.
