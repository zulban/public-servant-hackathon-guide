Une runsheet est une liste principale chronologique de tous les événements et heures clés de l'événement. Chaque élément d'une runsheet doit avoir une heure ou un intervalle de temps précis.

Cette page est:

* une liste des événements clés qui devraient tous figurer sur n'importe quelle runsheet du hackathon.
* une liste des rappels, des recommandations et des annonces faites tout au long de l'événement

Cette page n'est pas:

* une liste de chaque élément évident sur une runsheet réelle, comme le démarrage de la cérémonie de remise des prix

# Annonces

* Annoncez lorsque il reste 3 heures, et une heure, pour la limite de soumission.
* Annoncer 3 heures avant la date limite à laquelle les groupes doivent cesser de travailler sur leur projet et commencer à travailler sur la soumission. Surtout dans le cas d'une vidéo ou d'une présentation, les groupes peuvent sous-estimer énormément le temps nécessaire à leur montage.
* Annoncez tous les flux en direct une heure avant de commencer.

# Activité

* Une ou deux fois par jour, un mentor doit visiter toutes les tables et demander à chaque groupe s'il a besoin d'aide.
* Le dernier mentor à partir au nuit doit visiter toutes les tables avant de partir.

# Juger

* La première fois que les juges se réunissent au même endroit, demandez-leur de se présenter brièvement.

# Cérémonie de clôture

* Montrez visuellement le travail de toutes les équipes gagnantes.
* Accueillez les équipes gagnantes sur scène, si possible.
* Remercier les organisateurs, les participants, le lieu, tout le monde.
* Demandez à chacun de remplir le sondage post-événement.
* Fournir les informations de contact des organisateurs.
* Fournir un lien qui communiquera un résumé de l'événement peu après l'événement.
