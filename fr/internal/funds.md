Cette page liste des conseils utiles pour faciliter la gestion du financement et des sponsors.

# Aliments

Différents acteurs ou sponsors peuvent avoir des préférences de restauration. Par exemple, Environnement et Changement climatique Canada peut insister sur la durabilité environnementale.

# Prix

Il peut être difficile pour le gouvernement de distribuer explicitement les prix. Dans le Code de valeurs et d'éthique du secteur public, les fonctionnaires doivent non seulement être éthiques, mais aussi paraître éthiques. Cela n'a pas l'air bien au public s’il ya une photo d’un directeur ou d’un ministre portant un chèque géant.

Voici quelques solutions possibles:

* Pas de prix en espèces.
* Embaucher une entreprise du secteur privé pour avoir le contrôle complet des prix.
* Programmes de financement alternatifs, comme des micro-subventions.

# Lieu

Les entreprises technologiques fournissent souvent leur site gratuit pour les hackathons en tant que stratégie de recrutement et de marketing. Envisagez de trouver un lieu gratuit auprès de sponsors locaux.

# Sponsors

Le financement peut être utilisé avec plus de flexibilité s'il est obtenu de sponsors externes. Cependant, trouver des sponsors peut prendre un minimum de 6 mois.

Attention aux conflits d'intérêts. Assurez-vous que la déclaration de mission des sponsors est alignée sur votre [charte de projet](charter.md) et votre département.
