Cette page liste les idées de prix et décrit les problèmes à éviter. Voir aussi: [fonds](funds.md).

Tous les prix ne nécessitent pas une récompense énorme. Et tous les prix ne doivent pas être attribués. Toutefois, si un projet le mérite, une mention spéciale peut toujours être très inspirant pour une équipe qui travaille fort.

# Idées de prix spéciaux

* Meilleure visualisation
* Plus amusant
* Meilleur éducatif
* Meilleur lié à l'eau
* Utilisation des données la plus surprenante.
* Meilleure soumission d'étudiant
* Meilleure soumission de fonctionnaire

# Logiciel Libre

La meilleure solution libre peut signifier beaucoup de choses:

* le code du projet est libre mais il utilise des outils non-libre
* le code du projet est libre, mais il nécessite des systèmes non-libre
* le code du projet est libre
* le code de projet est FOSS et fonctionne sur des systèmes entièrement FOSS et libre

Il appartient aux organisateurs de décider comment répondre à ces critères.

Au minimum, ce prix devrait nécessiter un fichier `LICENSE` standard, disponible dans tout projets libre.
