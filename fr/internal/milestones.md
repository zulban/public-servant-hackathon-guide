Cette page décrit les principales étapes de la planification d’un hackathon. Chaque élément de cette page doit être fait et doit être fait dans cet ordre.

# 1. Charte de projet

Écrire une courte [charte du projet](charter.md) afin que tout le monde ait une compréhension commune de l'événement.

# 2. Équipe

Pour chaque [rôle](../roles) défini dans ce guide, décidez s'il sera géré:

* en externe par une entreprise privée
* à l'interne par un fonctionnaire

Pour les rôles internes, recherchez un fonctionnaire spécifique et confirmez qu'il est intéressé. Ajoutez cette information à la charte du projet.

# 3. Approbation

Partagez la charte du projet avec les principaux sponsors et parties prenantes et obtenez leur approbation.
