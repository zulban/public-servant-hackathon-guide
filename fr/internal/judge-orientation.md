Cette page décrit le document d'orientation à envoyer aux juges avant le début de l'événement.

# Basics

* Liste de contacts
* Où est l'événement?
* Quand est l'événement?
* Horaires minimum où les juges doivent être présents.
* Bref résumé des thèmes et des défis.
* Liste des prix.
* Comment se rendre au lieu, description du parking ou utiliser les transports en commun pour se rendre au lieu.

# Juger

* Résumé du processus de jugement et du horaire.
* Brève description de ce à quoi ressemblera une soumission.
* Brève description de la présentation ou du format de présentation, le cas échéant.
* Les critères de jugement et les tableaux ou rubriques utilisés pour juger les groupes.
