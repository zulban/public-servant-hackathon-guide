Les chartes de projet peuvent être longues et compliquées. Au lieu de cela, cette page décrit une charte de projet minimal pour un hackathon gouvernemental. Ajoutez les sections de votre choix, mais votre charte devrait tout inclure sur cette page.

# Définition

Dans la gestion de projet, une charte de projet, une définition de projet ou une déclaration de projet est une déclaration de la portée, des objectifs et des participants d'un projet. Il fournit une définition partagée d'un projet.

# Sections

## Objectif

Pourquoi planifier un hackathon? Pourquoi un hackathon? À quel besoin ou à quel mandat spécifique répond-on?

Voici quelques exemples. Il est préférable de se concentrer sur quelques-uns seulement:

* Promouvoir nos données ouvertes au public. (spécifiez quelles données ouvertes)
* Intéresser les participants à postuler à des emplois au gouvernement.
* Incitation à améliorer notre documentation.
* Obtenez de nouvelles idées innovantes des participants.
* Obtenez immédiatement des codes / prototypes utiles des participants. (peu probable)
* Exposition médiatique.

## Objectif mesurable

Comment mesurons-nous le succès quantitativement? Définir le succès et l'échec comme des résultats mesurables. Par exemple:

* Le succès est au moins X nombre de participants.
* Le succès est Y nombre de soumissions.
* Le succès est au moins un grand prototype / projet.
* Participants satisfaits d'après un sondage post-événement.
* Exposition des médias

## Public

Décrivez les participants que nous voulons en termes de:

* Tranche d'âge
* Niveau scolaire
* Niveau de compétence technique minimum
* Statut d'emploi
* Nationalité
* Fonctionnaires, pas les fonctionnaires, ou les deux
* Secteur privé
* Participants en personne, par opposition aux participants à distance

## Livrables du concours

Quel genre de soumissions voulons-nous voir au hackathon? Généralement:

* En utilisant quels données?
* Avec quels outils?
* Quelles restrictions, le cas échéant, sur les technologies utilisées?

## Des risques

* Qu'est-ce qui peut mal tourner et à quel point serait-il grave?

## Budget

Voir [fonds](funds.md).

* Quels sont les coûts estimés?
* Quel est le moins cher possible?
* Combien cela pourrait-il coûter?

## Sponsors

* Qui sont les sponsors susceptibles de financer l'événement?

## Les parties prenantes

Identifiez les groupes et les personnes qui:

* Sont intéressés par cet événement
* Devrait être averti de cet événement
* Peut être intéressé à soutenir et à planifier cet événement

## Approbation du projet

Voir [approbations](approbation.md).
