Cette page décrit les exigences minimales pour qu'une plate-forme de gestion de projet pour planifier un hackathon gouvernemental.

* Privé. Le contenu n'est visible que par les membres invités.
* Accessible en interne et en externe. Toute personne ayant un accès Internet peut être invitée à rejoindre la plateforme.
* Si possible, un logiciel libre (FOSS).

# Billets

* Tous les membres peuvent soumettre de nouveaux billets.
* Tous les billets ont un fil de discussion.
* Les membres peuvent attribuer des billets à d'autres membres.
* Les billets peuvent avoir des délais.
* Les membres peuvent voir différents sommaires de tous les billets, comme un diagramme de Gantt ou des problèmes en retard.

# Voir également

* [GitLab](https://gitlab.com)
