Cette page fournit des conseils sur la rédaction de la demande de propositions (RFP) et de l'énoncé de travail pour le recrutement d'une entreprise privée dans le cadre de l'organisation d'un hackathon gouvernemental.

Il se peut que vous receviez des formulaires à remplir qui sont similaires, mais non identiques, aux sections de cette page. Certains de ces exemples font référence à des documents dans ce "Guide du hackathon des fonctionnaires" et devront être inclus dans l'annexe de tout document juridique.

# Procédures d'évaluation

Les fonctionnaires doivent contacter brièvement les anciens clients des fournisseurs dans le cadre de l'évaluation requise critères pour les vendeurs.

# Énoncé de travail

### 4.0 Livrables, tâches et spécifications techniques

Le contractant assumera toutes les responsabilités pour tous les [rôles](../roles) définis dans le Guide du hackathon des fonctionnaires (voir annexe). Ceci inclut mais ne se limite pas à:

* Coordonner la logistique des sites, des aliments et des technologies.
* Jouer en tant que maître de cérémonie (MC).
* Partager des documents de planification avant le début de l'événement, comme une feuille de calcul et des scripts d'annonce.
* Planifier et exécuter un processus de jugement efficace avec des prix et des règles.
* Marketing avant et pendant l'événement.
* Les efforts de recrutement des participants.
* Les efforts de recrutement des mentors et leur orientation.
* Les efforts de recrutement des juges et leur orientation.
* Participer à un processus post-mortem à propos de l'événement.
* Fournir des représentants sur place pour toutes les heures de l'événement.
