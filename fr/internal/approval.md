Cette page décrit toutes les approbations pouvant être requises pour un hackathon gouvernemental.

* Direction générale des services ministériels et des finances (CSFB)
* Services partagés Canada (SPC)
* Bureau de l'éthique
* Office de la propriété intellectuelle (IPO)
* Communications
