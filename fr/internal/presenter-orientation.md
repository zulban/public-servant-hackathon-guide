Cette page décrit le document d'orientation à envoyer aux présentateurs avant le début de l'événement.

# Informations de base

* Liste de contacts
* Où est l'événement?
* Quand est l'événement?
* Les horaires minimaux où les animateurs doivent être présents. Si possible, demandez seulement aux présentateurs d’arriver pour leur heure de présentation et rien de plus.
* Comment se rendre au lieu, description du parking ou utiliser les transports en commun pour se rendre au lieu.

# En présentant

* Résumé de [qu'est-ce qu'un hackathon](../overview.md).
* Expliquez comment un hackthon n'est pas une conférence. Surtout s'il y a des prix en argent, la moitié de la salle n'écoute peut-être pas une présentation. Faites savoir aux présentateurs que c'est normal et que c'est bon.
