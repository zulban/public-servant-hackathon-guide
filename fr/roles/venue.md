Cette page décrit le rôle de "coordinateur de site".

Le coordonnateur du lieu s'assure que l'emplacement est suffisant et que de la nourriture est disponible.

### Responsabilités

Avant l'événement:

* Réservez le lieu(s).
* Réservez le traiteur(s).
* Coordonner le choix, l’achat et le transport des produits alimentaires.
* Assurez-vous que l'événement dispose de suffisamment de chaises, de tables et de postes de travail confortables.
* Écrire une liste de tout le matériel nécessaire à l'événement et des responsables de son apport.
* Travailler avec le [coordinateur technique](tech.md) en ce qui concerne l'électricité et Internet.
* Assurez-vous que le [gardien](custodian.md) dispose de toutes les informations essentielles sur le lieu et les traiteurs.

Pendant l'événement:

Aucun.

Après l'événement:

* Inclure des notes sur ce rôle dans le [post mortem](../internal/post-mortem.md).
