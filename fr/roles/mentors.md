Cette page décrit le rôle de "coordinateur de mentors".

Le coordinateur des mentors veille à ce que des mentors soient disponibles en ligne lors de l'événement et qu'ils sachent comment aider les participants.

### Responsabilités

Avant l'événement:

* Contactez des mentors potentiels, en leur demandant de vous aider. Contactez autant que possible car ils pourraient également devenir des participants réguliers.
* Fournir un atelier de formation de mentor court à tous les mentors.
* Envisagez de recruter des étudiants diplômés avec des diplômes liés à l'événement pour assister à l'événement en tant que ** mentors ** rémunérés. Ils peuvent naturellement inviter leurs amis ou participer.
* Travaillez avec le [coordinateur web](web.md) pour vous assurer qu'une liste de mentors est disponible en ligne. Cette liste peut être incomplète mais elle devrait exister.
* Partagez les noms, les photos, les compétences et l'organisation de tous les mentors sur ces pages, au fur et à mesure de leur disponibilité.
* Assurez-vous que la [runsheet](../internal/runsheet.md) a une heure pendant l'événement où tous les mentors sont présentés aux participants. Chaque mentor devrait avoir la possibilité de décrire ses compétences et son expertise.

Pendant l'événement:

* Informez les mentors que vous êtes le coordinateur des mentors et disponible pour répondre à toutes vos questions sur le mentorat.
* Travaillez avec le [coordinateur de la formation](training.md) pour vous assurer que les mentors ont accès à leur fiche d'informations.
* Assurer que les mentors ont pu assister et aider.
* Gardez la liste en ligne des mentors à jour (autant que possible).

Après l'événement:

* Inclure des notes sur ce rôle dans [post mortem](../ internal/post-mortem.md).
