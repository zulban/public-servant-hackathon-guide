Cette page décrit le rôle de "coordinateur marketing".

Le coordinateur marketing assure la promotion de l'événement avant, pendant et après l'événement.

### Responsabilités

Avant l'événement:

* Rédigez un dossier d’information sur l’événement pouvant être partagé avec les médias.
* Contactez le service des communications du gouvernement pour planifier les communications pour l'événement.
* Contactez les fonctionnaires avec les comptes de réseaux sociaux les plus populaires qui pourraient être intéressés par la promotion de l'événement.
* Contactez les communautés technologiques locales.
* Contacter les universités locales.
* Contacter des sponsors potentiels et les mettre en contact avec le [comité de planification](committee.md).
* Discutez avec le [coordinateur des mentors](mentors.md) de la rémunération des mentors qualifiés pour participer à la promotion de l'événement.
* Partager une liste marketing avec le [comité](comité.md). Ceci est une liste de tous les groupes et personnes que vous avez contactés ou prévoyez de contacter. Pensez à une plate-forme telle que Google Documents afin que la liste puisse être à jour sans qu'il soit nécessaire de renvoyer constamment un fichier par courrier électronique.

Pendant l'événement:

* Assurez-vous qu'il y a au moins cinquante photos de l'événement, prises tous les jours et pendant plusieurs heures. Au minimum, avec un téléphone intelligent de bonne qualité.
* Prenez une photo des interviews des médias, le cas échéant, au fur et à mesure qu'elles se déroulent (y compris une caméra, un microphone, etc.).

Après l'événement:

* Faites de votre mieux pour collecter des photos de l'événement, le cas échéant, des participants ou de toute autre personne.
* Inclure des notes sur ce rôle dans [post mortem](../internal/post-mortem.md).
