Cette page décrit le rôle de "coordinateur de présentations".

Le coordonnateur des présentations s'assure qu'il y a de bons présentateurs et qu'ils sont en mesure de faire des présentations.

### Responsabilités

Avant l'événement:

* Préparez un [formulaire de candidature du présentateur](../external/ presenter-application.md) et obtenez l’approbation du [comité](committee.md).
* Partagez le formulaire avec des experts, des universités, des entreprises et des communautés technologiques.
* Travaillez avec le [maître de cérémonie](mc.md) pour vous assurer que la [runsheet](../internal/runsheet.md) dispose de suffisamment de temps pour les présentations et les annonce à l'avance.
* Rédigez un horaire de présentation auquel le public pourra s’adresser et qui pourra être affiché sur un projecteur. Une page.
* Travaillez avec le [coordinateur web](web.md) pour vous assurer que le calendrier de la présentation possède sa propre page web sur le site web public.
* Travailler avec le [coordinateur de la formation](training.md) pour développer le [paquet d'orientation pour les présentateurs](../internal/presenter-orientation.md).

Pendant l'événement:

* Restez en contact avec les présentateurs. Soyez conscient des annulations ou des ajustements à l'horaire.
* Travaillez avec le [coordinateur technique](tech.md) pour vous assurer que l'équipement de présentation est prêt pour toutes les présentations.
* Assurez-vous que l'horaire des présentations est régulièrement affiché sur un projecteur, en particulier juste avant les présentations.
* Remercier chaque présentateur personnellement, avant et après leur présentation.

Après l'événement:

* Inclure des notes sur ce rôle dans [post mortem](../internal/post-mortem.md).
