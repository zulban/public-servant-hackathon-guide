Cette page décrit le rôle de "coordinateur de la formation".

Le coordinateur de la formation veille à ce que tous les groupes de personnes aient accès aux ressources et matériels de formation pertinents.

### Groupes de formation

Les groupes de formation sont:

* Fonctionnaires (présents ou non)
* Les mentors
* Juges
* Participants

### Responsabilités

Avant l'événement:

* Ecrire différents paquets d'information pour chaque groupe de formation.
* Partagez-les avant le début de l'événement.
* Rédigez une liste d’au moins vingt termes de vocabulaire et d’acronymes pouvant être utilisés lors de l'événement. Définissez-les et partagez la liste avec le [coordinateur web] (web.md) pour le partager en ligne.
* Produire un exemple pratique pouvant être achevé en moins de 5 minutes, démontrant l’utilisation de base de certains logiciels, outils ou données relatifs à l’événement.
* Travaillez avec le [gardien](custodian.md) pour vous assurer que tout le monde est au courant de toutes les politiques relatives aux événements, telles que le code de conduite.
* Organisez un atelier pratique pour les mentors, en leur faisant visiter les outils, les données et les plateformes sur lesquels ils pourraient être interrogés. Cela peut être fait juste avant ou au début de l'événement.
* Dans la mesure du possible, créez du matériel de formation générique pouvant être réutilisé.

Pendant l'événement:

* Assurez-vous que le matériel de formation pertinent reste disponible et annoncé pendant l'événement.

Après l'événement:

* Partagez le matériel de formation que vous avez créé avec de plus grandes communautés d’organisateurs de hackathon.
* Inclure des notes sur ce rôle dans [post mortem](../internal/post-mortem.md).
