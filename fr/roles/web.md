Cette page décrit le rôle de "coordinateur web".

Le coordinateur web s'assure que les médias et les informations sont disponibles sur le web, ainsi que des services web tels que les inscriptions, les soumissions et les travaux d'évaluation.

# Les pages web

### Liste des juges

Une page web séparée avec les noms, les photos, les compétences et l'organisation pour tous les **juges**, à mesure qu'elle devient disponible.

### Liste des mentors

Une page web séparée avec les noms, les photos, les compétences et l'organisation pour tous les **mentors**, à mesure qu'elle devient disponible.

### Informations clés

Chaque hackathon nécessite une page web répertoriant toutes les [informations de clé](../external/key-info.md).

# Responsabilités

Avant l'événement:

* Assurez-vous que toutes les pages web de ce document sont disponibles en ligne.
* Travailler avec le [coordonnateur du bilinguisme](biligualism.md) pour traduire certaines pages.
* Fournir les services web requis par le [coordinateur des juges]juges.md) pour le processus de jugement.
* Aidez le coordinateur marketing et le coordinateur de la formation à héberger leurs fichiers et leurs supports en ligne.
* Préparez un [formulaire de soumission web fonctionnel](../external/submission.md).

Pendant l'événement:

* Assurez-vous que le site web est disponible.

Après l'événement:

* Assurez-vous que tout le contenu multimédia (comme les vidéos et les flux) est accessible au public en ligne.
* Inclure des notes sur ce rôle dans [post mortem](../internal/post-mortem.md).
