Cette page décrit le rôle de "coordinateur d'inscription".

Le coordinateur des inscriptions s'assure que l'inscription à l'événement est simple, stable et disponible à temps.

### Responsabilités

Avant l'événement:

* Trouver ou déployer une plate-forme d'enregistrement capable d'accepter un nombre raisonnable d'inscriptions.
* Assurez-vous que le processus est simple.
* Assurez-vous que le processus est disponible au moins deux mois avant l'événement. Plus c'est mieux.
* Assurez-vous que le processus affiche et respecte toutes les règles et réglementations.
* Assurez-vous que le formulaire d'inscription répond à toutes les [conditions d'enregistrement](../external/registrations.md).

Pendant l'événement:

Aucun.

Après l'événement:

* Inclure des notes sur ce rôle dans le [post mortem](../internal/post-mortem.md).
