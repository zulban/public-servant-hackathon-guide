Cette page décrit le rôle de "chef de projet".

Le chef de projet supervise l'utilisation de la [plate-forme de gestion de projet](../internal/project management platform) et tient à jour une liste de tous les contacts pour l'événement.

# Responsabilités

Avant l'événement:

* Travaillez avec le [coordinateur web](web.md) pour sélectionner et déployer une [plate-forme de gestion de projet](../internal/project management platform).
* Appliquer de bons principes de gestion de projet au processus de planification.
* Aidez tous les membres de l'équipe à utiliser la plateforme.
* Assemblez les listes de contacts de tous les coordinateurs dans une liste de contacts principale.

Pendant l'événement:

* Aucun.

Après l'événement:

* Inclure des notes sur ce rôle dans le [post mortem](../internal/post-mortem.md).
