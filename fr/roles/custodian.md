Cette page décrit le rôle de "gardien".

Le gardien veille à ce que le lieu soit physiquement accessible et sûr pour tout le monde, à toutes les heures de l'événement.

### Responsabilités

Avant l'événement:

* Partager en interne une liste de contacts importants pour l'événement. Par exemple: la sécurité, les planificateurs d'événements, le personnel sur place et les propriétaires d'immeubles.
* Comprendre toutes les règles, restrictions et calendriers pour le bâtiment et pour la sécurité.
* Trouver ou rédiger un code de conduite, y compris un mécanisme de signalement des violations du code de conduite.
* Assurez-vous que le code de conduite est inclus dans les règles de l'événement et le processus d'inscription.

Pendant l'événement:

* Assurez-vous qu’une personne est disponible sur le site avec un accès physique au site à toute heure de la manifestation.
* Assurez-vous que l'existence du code de conduite est mentionnée dans certaines annonces.
* Assurez-vous que la restauration a lieu dans les délais.

Après l'événement:

* Inclure des notes sur ce rôle dans [post mortem](../internal/post-mortem.md).

# Voir également

* Exemple [code de conduite](https://codefordc.org/code-of-conduct.html) de "Code for DC".
