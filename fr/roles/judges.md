Cette page décrit le rôle de "coordinateur des juges".

Le coordinateur des juges s'assure qu'il y a des juges et qu'ils sont capables de choisir efficacement les gagnants.

### Responsabilités

Avant l'événement:

* Discutez avec le [comité](comité.md) pour savoir si les juges seront rémunérés.
* Décidez le format des soumissions. Sera-ce un pitch, une présentation, une soumission vidéo ou autre chose?
* Décidez si les soumissions à distance seront acceptées et, le cas échéant, si elles auront le même format que les soumissions en personne.
* Créer une liste de personnes qui pourraient être des juges.
* Contacter les juges potentiels, en leur offrant le rôle.
* Écrivez un processus et des critères de jugement réalistes et efficaces.
* S'assurer que le processus de jugement est correctement représenté dans les documents de planification et les documents juridiques.
* Collaborez avec le [coordinateur web](web.md) pour vous assurer qu'une liste de juges est disponible en ligne.
* Collaborez avec le [coordinateur de la formation](training.md) pour développer le [document de formation pour les juges](../internal/judge-orientation.md).
* Partagez les noms, les photos, les compétences et l'organisation de tous les juges sur ces pages, au fur et à mesure de leur disponibilité.
* Assurez-vous que la [runsheet](../internal/runsheet.md) a une heure pendant l'événement où les noms et les descriptions de tous les juges sont annoncés aux participants.

Pendant l'événement:

* Identifiez-vous en tant que juge coordinateur pour les juges.
* Assurez-vous que le processus de jugement se déroule sans heurts et à temps.
* Prendre des notes sur quelles juges ont pu assister et juger.
* Travaillez avec le [coordinateur de la formation](training.md) pour vous assurer que les juges ont accès à leur fiche d'informations.

Après l'événement:

* Inclure des notes sur ce rôle dans [post mortem](../internal/post-mortem.md).
