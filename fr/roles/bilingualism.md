Cette page décrit le rôle de "coordonnateur du bilinguisme".

Le coordonnateur du bilinguisme s'assure que les médias créés pour l'événement et l'événement lui-même sont raisonnablement bilingues.

### Responsabilités

Avant l'événement:

* Assurez-vous que tous les documents et supports publics créés pour l'événement sont traduits en anglais / français.

Pendant l'événement:

* Aide avec les annonces, les périodes de questions et les cérémonies si les présentateurs ne sont pas à l'aise dans une langue officielle.

Après l'événement:

* Assurez-vous que toutes les vidéos partagées en ligne ont un texte bilingue pour leur titre et leur description.
* Inclure des notes sur ce rôle dans le [post mortem](../ internal/post-mortem.md).
