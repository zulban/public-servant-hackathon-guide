Cette page décrit le rôle de "membre du comité organisateur".

Tous les membres du comité organisateur ont une vue d'ensemble de tous les aspects de la planification.

### Responsabilités

Avant l'événement:

* Programmer des réunions régulières.
* Assister à la plupart des réunions régulières.
* Participer aux discussions sur la [financement et sponsors](../internal/funds.md).
* Assurez-vous que tous les [rôles](../roles) ont une personne assignée, à tout moment.
* Participer aux discussions dans le [système de gestion de projet](../internal/project-management.md).

Pendant l'événement:

Aucun.

Après l'événement:

* Inclure des notes sur ce rôle dans [post mortem](../internal/post-mortem.md).
