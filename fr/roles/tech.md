Cette page décrit le rôle de "coordinateur de la technologie".

Le coordinateur de la technologie s'assure que l'événement dispose de l'électricité, de l'Internet, du matériel de présentation et du matériel technique.

### Responsabilités

Avant l'événement:

* Assurez-vous que le site dispose de suffisamment de bande passante internet et peut gérer un nombre suffisant d'appareils mobiles.
* Assurez-vous que l'événement contient suffisamment de barres d'alimentation et que les circuits électriques peuvent gérer de nombreux ordinateurs portables.
* Assurez-vous qu'aucune limite de pare-feu ou de bande passante n'empêchera les participants de visiter des sites web ou des services web importants. Notez que les participants peuvent tous avoir la même adresse IP, car ils utilisent la même connexion Internet.
* Assurez-vous que les présentateurs disposent d'un microphone et d'un projecteur, selon les besoins.
* Planifiez, si nécessaire, les conditions requises pour les flux vidéo ou l'enregistrement vidéo de l'événement.

Pendant l'événement:

* Assurez-vous que l'équipement pour la première présentation fonctionne correctement.
* Assurez-vous que les informations de connexion sans fil sont bien en évidence lors de l'événement. Envisagez d’imprimer les informations sur du papier et de les coller sur quelques murs.
* Les flux en direct et l'enregistrement vidéo (le cas échéant) fonctionne. Pensez aux périphériques d'enregistrement redondants et au stockage de sauvegarde.

Après l'événement:

* Inclure des notes sur ce rôle dans [post mortem] (../ internal/post-mortem.md).
