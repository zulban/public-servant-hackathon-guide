Cette page décrit le rôle de "maître de cérémonie" (MC).

Le maître de cérémonie fait toutes les annonces importantes, présente les présentateurs et aide à organiser la cérémonie de remise des prix.

### Responsabilités

Avant l'événement:

* Ecrire une [runsheet](../internal/runsheet.md) en fonction des besoins du [coordinateur de présentations](présentations.md) et du [comité de planification](committee.md).

Pendant l'événement:

* Assurez-vous que quelqu'un fait toutes les annonces et les introductions.
* Essayez de garder l'événement en cours à l'heure conformément à la runsheet.
* Notifier les organisateurs si la runsheet s'écarte considérablement.
* Assurez-vous que chaque groupe est remercié à plusieurs reprises dans ses annonces: juges, mentors, participants et organisateurs.

Après l'événement:
 
 * Inclure des notes sur ce rôle dans [post mortem](../internal/post-mortem.md).
