Cette page décrit le rôle de "coordinateur de prix".

Le coordinateur du prix veille à ce que les prix soient conformes à toutes les règles et soient attribués aux meilleures équipes dans un délai raisonnable.

### Responsabilités

Avant l'événement:

* Évaluez si les participants au hackathon sont réellement intéressés à gagner les prix. Ne présumez pas que les participants aimeront les prix parce que vous les aimez.
* Évaluez le processus de jugement sur la facilité avec laquelle une soumission frauduleuse peut gagner. Avertissez les organisateurs.
* Evaluer le processus de jugement sur la facilité avec laquelle un "citron" peut gagner. Un citron est un projet qui a une bonne première impression, mais à y regarder de plus près, il n’a aucune valeur. Avertissez les organisateurs.
* Assurez-vous que le processus de remise des prix peut être adapté de manière à ce qu'il n'y en ait pas trop ou pas trop.

Pendant l'événement:

* Pensez à annuler les prix ou à fusionner les catégories s'il n'y a pas assez de propositions dans une catégorie.
* Envisagez d'ajouter des prix honorifiques à la volée, comme "meilleure vision artistique" pour des contributions remarquables qui, autrement, ne gagneraient rien. Travaillez avec le [maître de cérémonie](mc.md) pour faire ces annonces spéciales.

Après l'événement:

* Assurez-vous que les gagnants reçoivent leurs prix ou leur argent en un temps raisonnable.
* Planifiez des rappels que vous devez suivre avec insistance, quel que soit le processus impliqué dans l'attribution des prix.
* Inclure des notes sur ce rôle dans [post mortem](../internal/post-mortem.md).
