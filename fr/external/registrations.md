Cette page liste tout ce qui doit être demandé dans un formulaire d'inscription.

### Basics

* Nom complet
* Adresse électronique
* Est-ce votre premier hackathon?
* Y participerez-vous en personne ou à distance?
* Êtes-vous un fonctionnaire?
* Êtes-vous un étudiant?

### Occupation

> Quel est votre titre de poste ou votre sujet d'étude?

Laissez la question ouverte pour donner aux étudiants la possibilité de répondre également.

### Commercialisation

> Comment avez-vous entendu parler de l'événement?

Si vous fournissez une liste d'options, assurez-vous de fournir une option pour "autre" où ils peuvent taper une réponse.

* Twitter
* Facebook
* LinkedIN
* Reddit
* Email interne du bulletin
* Bouche à oreille
* Autre (veuillez préciser)

etc.

### Accessibilité

> Avez-vous des besoins ou des demandes d’accessibilité particuliers?

Une réponse ouverte, zone de texte.
