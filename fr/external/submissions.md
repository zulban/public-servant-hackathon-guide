Cette page liste tout ce qui doit être demandé dans une soumission de projet.

Fournir un avertissement sur la soumission pour que les soumissions **puissent** être rejetées si elles ne respectent pas ces exigences.

### Basics

* Titre du projet
* Résumé du projet (maximum 100 mots)
* Nom de l'équipe

### Prix

Une liste de contrôle de tous les prix, thèmes et catégories auxquels ce projet est éligible. Les participants choisissent les prix pour lesquels ils souhaitent que les juges les considèrent. Par exemple:

* Thème 2: Trouver de nouveaux publics
* Ce projet est open source
* Ceci est un projet étudiant

### Des dossiers

* Un lien vers un dépôt git accessible au public.

Pour tous les fichiers ou dossiers, indiquez clairement:

* s'il a été créé par l'équipe **pendant** le hackathon
* s'il a été créé par l'équipe **avant** le hackathon
* s'il a été créé par d'autres, comme une bibliothèque ou un outil open source

Les participants peuvent enregistrer cela dans un fichier appelé «rapport d'originalité» dans leur projet.

Remarque: si un dossier est décrit, il n'est pas nécessaire de décrire ce qu'il contient.

### Description détaillée

Tous les projets doivent comporter au moins l'un des éléments suivants:

1. Un lien vers une vidéo (maximum 2 minutes).
1. Un lien vers une page Web (la page principale peut être lue en 2 minutes)
1. Un lien vers un document (le contenu principal peut être lu en 2 minutes)

La description détaillée doit montrer et expliquer tous les résultats produits par le projet.
