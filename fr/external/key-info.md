Chaque hackathon nécessite une page Web (ou des pages Web) listant toutes les informations critiques. Cette page liste tous les éléments devant figurer sur cette page. Sauf indication contraire, chaque question doit être répondue en **une** phrase.

Voir [ici](/fr/examples/key-info.md) pour un exemple. Cette page a été inspirée par l'événement [GEOSS / HACK 2018] (http://www.earthobservations.org/me_201805_dpw.php?t=hackathon).

# Basics

### Comment ça fonctionne

Combien de temps dure l'événement et ce que les gens sont censés faire.

### Les défis

Lorsque le ou les défis ou le ou les thèmes seront annoncés, et en gros quels seront ces défis et ce ou ces thèmes.

### Les [prix](../prizes.md)

En 1 à 3 phrases, décrivez tous les prix, honneurs, récompenses, offres d’emploi et conseils attribués aux gagnants.

# Questions & Réponses

### Qu'est-ce qu'un hackathon?

### Quels outils puis-je utiliser?

Décrivez les langages de programmation et les outils logiciels que les participants sont autorisés à utiliser. Si la réponse est "tous", vous devez le leur faire savoir.

### Quel est le format du produit que les participants vont livrer?

Cela peut être un pitch, une vidéo, un code, etc, ou autre chose.

### Comment fonctionnera le jugement?

Énumérer les critères de jugement et quand le jugement aura lieu.

### Qui peut participer?

### Que devrais-je apporter?

### Cet événement est-il gratuit?

Il est difficile de faire la différence entre "événement gratuit" et "Je ne trouve pas comment acheter un billet". Si l'événement est gratuit, dites-le.

### Comment les participants à distance vont-ils participer?

Décrivez comment, ou si ce n'est pas possible, dites-le.

### Quel est le calendrier de l'événement?

Indiquez l'heure et la date de début, l'heure et la date de fin et le moment où les résultats seront annoncés.

### Puis-je dormir à l'événement?

Certains hackathons permettent aux participants de dormir pendant l'événement. Dites si c'est possible ou non.

# Logistique

### Où

Décrivez non seulement l'adresse physique, mais également toutes les plateformes en ligne sur lesquelles l'événement a lieu.

### Quand

Listez le:

* date et heure de début
* date et heure de fin

### Contact

Indiquez les numéros de téléphone et les adresses électroniques de tous les [rôles publics](../roles/readme.md). Décrivez les raisons pour contacter chaque personne.
