Cette page contient un exemple de formulaire d'inscription pour trouver des présentateurs.

Si vous contactez plusieurs startups ou étudiants, nous vous recommandons ces étapes pour vous aider à trouver les présentateurs de la meilleure qualité. Ils sont moins nécessaires si vous contactez une entreprise bien établie.

# Formulaire de demande

Vous pouvez demander aux présentateurs potentiels de remplir un formulaire en ligne, tel qu'un Google Form. Vous pouvez également simplement inclure ces questions dans le courrier électronique.

* Nom complet du représentant
* Adresse électronique.
* Fournissez un lien vers une image ou une courte vidéo montrant un prototype réel, un projet ou une entreprise en rapport avec notre événement.
* Combien de présentations publiques avez-vous données?
* Une ou deux phrases (maximum) décrivant votre prototype, projet ou activité.
* Liens supplémentaires
