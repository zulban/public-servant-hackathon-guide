[(English)](../en/readme.md)

# Objectif

Ce guide aide les fonctionnaires à organiser des hackathons au gouvernement. Vous pouvez commencer en lisant le [sommaire](overview.md).

# Qu'est-ce que ce projet est

C'est un bon endroit pour:

* des exemples concrets de documents gouvernementaux, tels que les conditions d'utilisation ou une [RFP](internal/rfp.md).
* des liens vers des ressources sur la planification de hackathons génériques
* listes de contrôle des objets à faire et quand les faire
* description des différents [rôles](roles) avec des listes de contrôle de leurs responsabilités

# Ce que ce projet n'est pas

Ce n'est pas le bon endroit pour:

* longues descriptions
* des instructions détaillées pour les hackathons non gouvernementaux (fournissez plutôt des liens)
* justifier pourquoi les hackathons du gouvernement sont une bonne idée
* données privées, protégées ou secrètes

# Pourquoi un repo?

Ce guide est un [git](https://git-scm.com/) repo de fichiers de démarques simples car nous pouvons:

* faire des changements en groupe et discuter facilement des changements.
* utiliser ces fichiers pour générer d'autres fichiers tels que PDF ou HTML.
* migrer les plates-formes facilement.
* comparer facilement une version à une autre, ligne par ligne.
