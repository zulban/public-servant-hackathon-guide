#!/usr/bin/python3

"""
This generates the markdown that lists all roles.
"""

import os

def get_line(path):
    with open(path,"r") as f:
        data=f.read()

    # assume the label of this coordinator type is in the first double quoted text
    label=data.split("\"")[1]
    label=label.capitalize()

    filename=path.split(os.sep)[-1]
    return f"* [{label}]({filename})"

source="../en/roles"
lines=[]
for filename in sorted(os.listdir(source)):
    if "readme.md" in filename:
        continue
    path=source+os.sep+filename

    try:
        lines.append(get_line(path))
    except IndexError:
        print(f"Failed to process file: '{filename}'")

print("\n\n"+"\n".join(sorted(lines)))
