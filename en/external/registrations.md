This page lists everything that needs to be asked in a registration form.

### Basics

* Full name
* Email address
* Is this your first hackathon?
* Will you be attending in person, or remotely?
* Are you a public servant?
* Are you a student?

### Occupation

> What is your job title or subject of study?

Leave the question open ended to give students an option to respond too.

### Marketing

> How did you hear about the event?

If you provide a list of options, make sure you provide an option for "other" where they can type a response.

* Twitter
* Facebook
* LinkedIN
* Reddit
* Internal newsletter email
* Word of mouth
* other (please specify)

etc.

### Accessibility

> Do you have any special accessibility needs or requests?

An open ended answer, text box.
