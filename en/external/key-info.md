Every hackathon needs a web page (or web pages) listing all critical information. This page lists every item that should be coverred on that page. Unless otherwise specified, each question should be answered in just **one** sentence.

See [here](en/examples/key-info.md) for an example. This page was inspired by the [GEOSS/HACK 2018](http://www.earthobservations.org/me_201805_dpw.php?t=hackathon) event.

# Basics

### How it works

How long the event is and what people are expected to do.

### The challenges

When the challenge(s) or theme(s) will be announced, and roughly what those challenge(s) and theme(s) will be.

### The [prizes](../prizes.md)

In 1-3 sentences, describe all prizes, honours, awards, job opportunities, and consulting awarded to the winners.

# FAQ

### What's a hackathon?

### What tools can I use?

Describe the programming languages and software tools participants are allowed to use. If the answer is "all", you need to let them know.

### What is the format of the product participants will deliver?

This might be a pitch, a video, code, all of these, or something else.

### How will the judging work?

List the judging criteria and when judging will take place.

### Who can attend?

### What should I bring?

### Is this event free?

It's hard to tell the difference between "free event" and "I cannot find how to buy a ticket". If free, say so.

### How will remote participants join in?

Describe how, or if it's not possible, say so.

### What's the event schedule?

List the start time and date, end time and date, and when results will be announced.

### Can I sleep at the event?

Some hackathons allows participants to sleep at the event. Say whether or not this is possible.

# Logistics

### Where

Describe not only the physical address, but any online platforms where the event takes place.

### When

List the:

* start date and time
* end date and time

### Contact

Provide the phone numbers and email addresses of all public facing [roles](../roles/readme.md). Describe the reasons to contact each person.
