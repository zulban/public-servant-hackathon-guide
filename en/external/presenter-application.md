This page has an example registration form to find presenters.

If you are contacting many startups or students, these steps are recommended to help find the best quality presenters. They are less necessary if you are contacting a well established business. 

# Application Form

You can get potential presenters to fill out an online form, like a Google Form. You might also simply include these questions in the email.

* ​Full name(s) of representative(s)
* Email address.
* Provide a link to an image or short video showing a real prototype, project, or business related to our event.
* How many public presentations you have given?
* One or two sentences (maximum) describing your prototype, project, or business.
* Additional links
