This page lists everything that needs to be asked in a project submission.

Provide a warning on the submission for that submissions **may** be disqualified if they don't follow these requirements.

### Basics

* Project title
* Project summary text (maximum 100 words)
* Team name

### Prizes

A checklist of all prizes, themes, and categories this project qualifies for. Participants check off what prizes they want judges to consider them for. For example:

* Theme 1: Artify the Earth
* Theme 2: Finding New Audiences
* This project is open source
* This is a student project

### Files

* A link to a publicly accessible git repo.

For all files or folders, clearly indicate:

* if it was created by the team **during** the hackathon
* if it was created by the team **before** the hackathon
* if it was created by others, like a library or open source tool

Participants can save this as a file called `originality-report` in their project.

Note: if a folder is described, there is no need to describe what it contains.

### Detailed Description

All projects must provide at least one of the following:

1. A link to a video (maximum 2 minutes).
1. A link to a webpage (the main page can be read in 2 minutes)
1. A link to a document (the main content can be read in 2 minutes)

The detailed description should show and explain all results produced by the project.
