This folder contains real examples of materials from past events.

Every event gets its own folder with the year, month, and name of the event.

```
2019-07-meteohack
```

# Open Formats

Wherever possible, share files in an open format, according to the [Government of Canada open data principles](https://open.canada.ca/en/open-data-principles).
