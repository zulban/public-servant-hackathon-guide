[(Français)](../fr/readme.md)

# Purpose

This guide helps public servants organize government hackathons. You can get started by reading the [overview](overview.md).

# What this project is

This is a good place for:

* real examples of government documents, like Terms of Use or an [RFP](internal/rfp.md).
* links to resources about planning generic hackathons
* checklists of items to do and when to do them
* descriptions of the different [roles](roles) with checklists of their responsibilities

# What this project is not

This is not the right place for:

* lengthy descriptions
* detailed guidance for non-government hackathons (provide links instead)
* justifying why government hackathons are a good idea
* private, protected, or secret data

# Why a repo?

This guide is a [git](https://git-scm.com/) repo of simple markdown files because we can:

* make changes as a group and discuss changes easily.
* use these files to generate other files like PDF or HTML.
* migrate platforms easily.
* easily compare one version to another, line by line.
