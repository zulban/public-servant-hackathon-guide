This page describes the role of "training coordinator". 

The training coordinator ensures that all groups of people have access to relevant resources and training materials.

### Training Groups

The training groups are:

* Public servants (in attendance, or not)
* Mentors
* Judges
* Participants

### Responsibilities

Before the event:

* Write different information packages for each training group.
* Share them before the event starts.
* Write a list of at least twenty vocabulary terms and acronyms that may be used at the event. Define them, and share the list with the [web coordinator](web.md) to share online.
* Produce one hands-on tutorial that can be completed in under 5 minutes, demonstrating basic usage of some software, tools, or data relevant to the event.
* Work with the [custodian](custodian.md) to ensure that everyone is aware of all event policies like the code of conduct.
* Provide a hands-on workshop for mentors, giving them a tour of the tools, data, and platforms they may be asked about. This can be done just before or at the start of the event.
* As much as possible, create generic training material that can be re-used.

During the event:

* Ensure that relevant training materials remain available and advertised during the event.

After the event:

* Share any training materials you created with larger communities of hackathon organisers.
* Include notes about this role in the [post mortem](../internal/post-mortem.md).
