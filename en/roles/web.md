This page describes the role of "web coordinator". 

The web coordinator ensures that media and information is available on the web, and web services like registrations, submissions, and judging works.

# Web Pages

### List of judges

A separate web page with the names, photos, skills, and organisation for all **judges**, as it becomes available to us.

### List of mentors

A separate web page with the names, photos, skills, and organisation for all **mentors**, as it becomes available to us.

### Key Info

Every hackathon needs a web page listing all [key information](../external/key-info.md).

# Responsibilities

Before the event:

* Ensure all web pages in this document are available publicly online.
* Work with the [bilingualism coordinator](biligualism.md) to translate some pages.
* Provide web services required by the [judge coordinator](judges.md) for the judging process.
* Help the marketing coordinator and training coordinator host their files and media online.
* Prepare a functional web [submission form](../external/submissions.md).

During the event:

* Ensure the web site is available.

After the event:

* Ensure that all media content (like videos and streams) are publicly available online.
* Include notes about this role in the [post mortem](../internal/post-mortem.md).