This page describes the role of "master of ceremonies" (MC). 

The master of ceremonies makes all major announcements, introduces presenters, and helps run the award ceremony.

### Responsibilities

Before the event:

* Write a complete and thorough [runsheet](../internal/runsheet.md) according to the needs of the [presentations coordinator](presentations.md) and [planning committee](committee.md).

During the event:

* Ensure that someone makes all announcements and introductions.
* Try to keep the event running on time according to the runsheet.
* Notify organizers if the runsheet is significantly straying from the runsheet.
* Ensure that each group is thanked multiple times in announcements: judges, mentors, participants, organisers.

After the event:
 
 * Include notes about this role in the [post mortem](../internal/post-mortem.md).
