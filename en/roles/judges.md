This page describes the role of "judge coordinator". 

The judge coordinator ensures there are judges, and they are able to effectively choose winners.

### Responsibilities

Before the event:

* Discuss with the [committee](committee.md) whether judges will be paid.
* Decide the format of the submissions. Will it be a pitch, presentation, video submission, or something else?
* Decide whether remote submissions will be accepted, and if so, if they will have the same format as in-person submissions.
* Create a list of people who might be judges.
* Contact potential judges, offering them the role.
* Write a judging process, and judging criteria, that is realistic and effective.
* Ensure the judging process is accurately represented in planning documents and legal documents.
* Work with the [web coordinator](web.md) to ensure that a list of judges are available online.
* Work with the [training coordinatory](training.md) to develop the [orientation package for judges](../internal/judge-orientation.md).
* Share the names, photos, skills, and organisation for all judges on these pages, as it becomes available to us.
* Ensure that the [runsheet](../internal/runsheet.md) has a time during the event where the names and descriptions of all judges are announced to participants.

During the event:

* Identify yourself as the judge coordinator to the judges.
* Ensure the judging process runs smoothly and on time.
* Take notes on which judges were able to attend and judge.
* Work with the [training coordinator](training.md) to ensure that judges have access to their information sheet.

After the event:

* Include notes about this role in the [post mortem](../internal/post-mortem.md).
