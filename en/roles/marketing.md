This page describes the role of "marketing coordinator". 

The marketing coordinator promotes the event before, during, and after the event.

### Responsibilities

Before the event:

* Write an information package about the event that can be shared with media outlets.
* Contact the government communications department to plan communications for the event.
* Contact public servants with the most popular social media accounts who might be interested in promoting the event.
* Contact local tech communities.
* Contact local universities.
* Contact potential sponsors, and put them in touch with the [planning committee](committee.md).
* Speak with the [mentor coordinator](mentors.md) about paying skilled mentors to attend, to promote the event.
* Share a marketing master list with the [committee](committee.md). This is a list of all groups and people you have contacted or plan on contacting. Consider a platform like Google Docs so that the list can be up to date without re-emailing a file constantly.

During the event:

* Ensure there are at least fifty photos of the event, taken on all days and over several hours. At a minimum, with a good quality smart phone.
* Take a photo of any news media interviews, if any, as they take place (including camera, microphone, etc).

After the event:

* Give a best effort to collect photos of the event, if any, from participants or anyone else.
* Include notes about this role in the [post mortem](../internal/post-mortem.md).
