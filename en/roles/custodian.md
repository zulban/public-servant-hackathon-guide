This page describes the role of "custodian". 

The custodian ensures that the venue is physically accessible and safe for everyone, for all hours of the event.

### Responsibilities

Before the event:

* Internally share a list of important contacts for the event. For example: security, event planners, on-site staff, and the building owners.
* Understand all rules, restrictions and schedules for the building and for security.
* Find or write a code of conduct, including a reporting mechanism for violations of the code of conduct.
* Ensure that the code of conduct is included in the event rules and signup process.

During the event:

* Ensure that at all hours of the event, someone is available on site with physical access to the venue.
* Ensure that the existance of the code of conduct is mentioned in some announcements.
* Ensure that catering occurs on schedule.

After the event:

* Include notes about this role in the [post mortem](../internal/post-mortem.md).

# See Also

* Example [code of conduct](https://codefordc.org/code-of-conduct.html) from Code for DC.
