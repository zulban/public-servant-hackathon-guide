This page describes the role of "prize coordinator". 

The prize coordinator ensures the prizes follow all rules and that they are awarded to the best teams in a reasonable amount of time.

### Responsibilities

Before the event:

* Evaluate whether hackathon participants are actually interested in winning the prizes. Don't assume participants will like the prizes because you do.
* Evaluate the judging process on how easy it may be for fraudulent submissions to win. Notify the organisers.
* Evaluate the judging process on how easy it may be for a "lemon" to win. A lemon is a project that has a great first impression and looks great, but upon closer examination has no value. Notify the organisers.
* Ensure that prizes process can adapt so there are not too few, or too many.

During the event:

* Consider cancelling prizes, or merging categories, if there are too few submissions in a prize category.
* Consider adding honorary prizes on the fly, like "best artistic vision" for notable submissions that otherwise would not win anything. Work with the [master of ceremonies](mc.md) to make these special announcements.

After the event:

* Ensure that winners receive their prizes or prize money in a reasonable amount of time.
* Schedule reminders for yourself to followup insistently with whatever processes are involved in awarding prizes.
* Include notes about this role in the [post mortem](../internal/post-mortem.md).
