This page describes the role of "mentor coordinator". 

The mentor coordinator ensures there are mentors at the event and available online, and that mentors understand how to help participants.

### Responsibilities

Before the event:

* Contact potential mentors, asking them to help. Contact as many as possible because they might also become regular participants.
* Provide a short mentor training workshop to all mentors.
* Consider recruiting graduate students with event-related degrees to attend the event as **paid** mentors. They may naturally invite their friends or participate.
* Work with the [web coordinator](web.md) to ensure that a list of mentors available online. This list may be incomplete but it should exist.
* Share the names, photos, skills, and organisation for all mentors on these pages, as it becomes available to us.
* Ensure that the [runsheet](../internal/runsheet.md) has a time during the event where all mentors are introduced to participants. Each mentor should get a chance to describe their skills and expertise.

During the event:

* Let the mentors know you are the mentor coordinator, and available to answer any questions about mentoring.
* Work with the [training coordinator](training.md) to ensure that mentors have access to their information sheet.
* Track which mentors were able to attend and help.
* Keep the online list of mentors up to date (as much as possible).

After the event:

* Include notes about this role in the [post mortem](../internal/post-mortem.md).
