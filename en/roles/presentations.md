This page describes the role of "presentations coordinator". 

The presentations coordinator ensures there are good presenters, and they are able to present.

### Responsibilities

Before the event:

* Prepare a [presenter application form](../external/presenter-application.md) and get approval from the [committee](committee.md).
* Share the form with experts, universities, businesses, and tech communities.
* Work with the [master of ceremonies](mc.md) to ensure that the [runsheet](../internal/runsheet.md) has time for presentations, and announces them in advance.
* Write a public facing presentation schedule that can be shown on a projector. One page or one slide.
* Work with the [web coordinator](web.md) to ensure the presentation schedule has its own web page on the public web site.
* Work with the [training coordinatory](training.md) to develop the [orientation package for presenters](../internal/presenter-orientation.md).

During the event:

* Stay in contact with presenters. Be aware of cancellations or adjustments to the schedule.
* Work with the [tech coordinator](tech.md) to ensure that presentation equipment is ready for all presentations.
* Ensure the presentation schedule is regularly shown on a projector, especially just before presentations.
* Thank each presenter personally, before and after they present.

After the event:

* Include notes about this role in the [post mortem](../internal/post-mortem.md).
