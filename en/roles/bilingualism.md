This page describes the role of "bilingualism coordinator". 

The bilingualism coordinator ensures that media created for the event and the event itself is all reasonably bilingual.

### Responsibilities

Before the event:

* Ensure that all public documents and media created for the event have English/French translations.

During the event:

* Help with announcements, question periods, and ceremonies if the presenters are not comfortable in one language.

After the event:

* Ensure that all videos shared online have bilingual text for their title and description.
* Include notes about this role in the [post mortem](../internal/post-mortem.md).
