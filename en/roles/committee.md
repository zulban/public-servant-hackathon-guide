This page describes the role of "organizing committee member". 

All members of the organizing committee have a high level view of all aspects of planning.

### Responsibilities

Before the event:

* Schedule regular meetings.
* Attend most regular meetings.
* Arrange for [funding and sponsors](../internal/funds.md).
* Ensure that all [roles](../roles) have an assigned person, at all times.
* Participate in discussions in the [project management system](../internal/project-management.md).

During the event:

None.

After the event:

* Include notes about this role in the [post mortem](../internal/post-mortem.md).
