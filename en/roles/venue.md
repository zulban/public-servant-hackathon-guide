This page describes the role of "venue coordinator". 

The venue coordinator ensures that the location is sufficient and food is available.

### Responsibilities

Before the event:

* Book the venue(s).
* Book the caterer(s).
* Coordinate the choosing, purchasing, and transportation of other food items.
* Ensure the event has enough chairs, tables, and comfortable workstations.
* Write a master list of all materials that are needed at the event and who is responsible for bringing them.
* Work with the [tech coordinator](tech.md) regarding electricity and internet.
* Ensure the [custodian](custodian.md) has all critical information about the venue and caterers.

During the event:

None.

After the event:

* Include notes about this role in the [post mortem](../internal/post-mortem.md).
