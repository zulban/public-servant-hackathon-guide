This page describes the role of "project manager". 

The project manager supervises the use of the [project management platform](../internal/project management platform) and maintains a list of all contacts for the event.

# Responsibilities

Before the event:

* Work with the [web coordinator](judges.md) to select and deploy a [project management platform](../internal/project management platform).
* Apply good project management principles to the planning process.
* Help all members of the team use the platform.
* Assemble contact lists from all coordinators into one master contact list.

During the event:

* None.

After the event:

* Include notes about this role in the [post mortem](../internal/post-mortem.md).
