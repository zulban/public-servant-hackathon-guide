This page describes the role of "registration coordinator". 

The registration coordinator ensures that signing up for the event is simple, stable, and available on time.

### Responsibilities

Before the event:

* Find or deploy a registration platform capable of accepting any reasonable number of signups.
* Ensure the process is simple.
* Ensure the process is available two months in advance of the event, at a minimum. More is better.
* Ensure the process displays and follows all rules and regulations.
* Ensure the registration form meets all [registration requirements](../external/registrations.md).

During the event:

None.

After the event:

* Include notes about this role in the [post mortem](../internal/post-mortem.md).
