This page describes the role of "technology coordinator". 

The technology coordinator ensures the event has electricity, internet, presentation equipment, and hardware.

### Responsibilities

Before the event:

* Ensure the venue has enough bandwidth and can handle enough mobile devices.
* Ensure the event has enough power bars and the electrical circuits can handle many laptops.
* Ensure that no firewall or bandwidth limit will block participants from visiting important web sites or web services. Note that participants may all have the same IP, due to using the same internet connection.
* Ensure that presenters have a microphone and projector, as needed.
* Plan, if necessary, the requirements for video streams or video recording the event.

During the event:

* Ensure the equipment for the first presentation works smoothly.
* Ensure that wireless connection information is prominently displayed at the event. Consider printing the information on paper and sticking it to a few walls.
* Live streams and video recording (if any) works. Consider redundant recording devices, and backup storage.

After the event:

* Include notes about this role in the [post mortem](../internal/post-mortem.md).
