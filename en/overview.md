[(Français)](../fr/overview.md)

# What is a hackathon?

A [hackathon](https://en.wikipedia.org/wiki/Hackathon) is a design sprint-like event in which computer programmers, graphic designers, interface designers, project managers, and others, often including domain experts, collaborate intensively on software projects.

A hackathon is not a conference or a convention. Almost all time in the schedule is empty because people need time to build interesting things.

Prizes in hackathons are great because lots of people will want to join our event and put in lots of effort. However prizes also give an incentive not to attend our live presentations and roundtables.

Almost all projects created in a hackthon are bad. Many are abandoned and never submitted. Most submissions are bad. That’s okay. Hackathons are about experimentation, learning, and making fast prototypes.

Hackathons have mentors and experts (sometimes with tables/booths). There needs to be lots of mentors present at a hackathon, ready to help.

# Milestones

See [milestones](internal/milestones.md) for a high level, chronological view of the highest priority tasks.

# Internal versus External

Government hackathons can be organised:

* Entirely by public servants
* Entirely by the private sector
* Partly by public, partly private

Early on you'll need to decide which [roles](roles) will be handled internally versus externally.

For example, you may want the marketing coordinator and master of ceremonies to be done by the public sector, but every other role is in the private sector contract.

# Roles

It's crucial that everyone knows their roles and are happy with them. People can be assigned to many roles, but it's recommended that each role only has one assigned person. Otherwise, if everyone is responsible for everything, nobody feels responsible for anything.

Roles also make it easy to re-assign people as necessary.

* [Bilingualism coordinator](roles/bilingualism.md)
* [Custodian](roles/custodian.md)
* [Judge coordinator](roles/judges.md)
* [Marketing coordinator](roles/marketing.md)
* [Master of ceremonies](roles/mc.md)
* [Mentor coordinator](roles/mentors.md)
* [Organizing committee member](roles/committee.md)
* [Presentations coordinator](roles/presentations.md)
* [Prize coordinator](roles/prizes.md)
* [Registration coordinator](roles/registration.md)
* [Technology coordinator](roles/tech.md)
* [Training coordinator](roles/training.md)
* [Venue coordinator](roles/venue.md)
* [Web coordinator](roles/web.md)

# Examples

You can find several examples of real documents [here](examples).

# Principles

Here are some important government of Canada principles that can govern your hackathon:

* [Open as a Foundation for Digital Government](https://open.canada.ca/en/blog/open-foundation-digital-government) which recommends open formats for all slides, documents, and data.
* [Open Data Principles](https://open.canada.ca/en/open-data-principles) which introduces and explains open data.
* [TBS Directive on Management of Information Technology](https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=15249) which requests that public servants consider open source solutions first, and release code written by public servants as open source by default.

# See Also

If you're serious about planning a hackathon, it is a good idea to read all of these resources.

* [hackathon.guide](https://hackathon.guide/)
* [A checklist for organizing a hackathon](http://www.agr.gc.ca/eng/about-us/planning-and-reporting/public-service-renewal-results-for-agriculture-and-agri-food-canada/data-hackathon/a-checklist-for-organizing-a-hackathon/?id=1542987317998) from Agriculture and Agri-Food Canada.
* [How to Manage Large Volunteer Hackathons](https://sunlightfoundation.com/2010/01/19/how-manage-large-volunteer-hackathons/)
