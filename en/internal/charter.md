Project charters can be long and complicated. Instead, this page describes a minimal project charter for a government hackathon. Add whatever sections you like, but your charter should include everything on this page.

# Definition

In project management, a [project charter](https://en.wikipedia.org/wiki/Project_charter), project definition, or project statement is a statement of the scope, objectives, and participants in a project. It provides a shared definition of a project.

# Sections

## Purpose

Why plan a hackathon? Why a hackathon? What need(s) or specific mandate(s) does this satisfy?

Here are some examples. It is best to focus on just a few:

* Promote our open data to the public. (specify which open data)
* Get participants interested in applying to government jobs.
* Incentive to improve our documentation.
* Get new innovative ideas from participants.
* Get immediately useful code / prototypes from participants. (unlikely)
* Media exposure.

## Measurable Objective

How do we measure success quantitatively? Define success and failure as measurable outcomes. For example:

* Success is at least X number of participants.
* Success is Y number of submissions.
* Success is at least one great prototype/project.
* Satisfied attendees based on a post-event survey.
* Media exposure in outlets X, Y, Z.

## Audience

Describe the participants we want in terms of:

* age range
* educational background
* minimum technical skill level
* employment status
* nationality
* public servants, not public servants, or both
* private industry
* in-person attendees, versus remote attendees

## Contest Deliverables

What kind of submissions do we want to see in the hackathon? Generally:

* using which datasets?
* using which tools?
* what restrictions, if any, on technologies used?

## Risks

* What can go wrong, and how bad would it be?

## Budget

See [funds](funds.md).

* What are the estimated costs?
* What is the cheapest this could be?
* How expensive might this become?

## Sponsors

* Who are the likely sponsors that will fund the event?

## Stakeholders

Identify the groups and people who:

* are interested in this event
* should be notified about this event
* may be interested in supporting and planning this event

## Project Approval

See [approvals](approval.md).
