This page provides guidance on writing the Request for Proposals (RFP) and Statement of Work for hiring a private company to help run a government hackathon.

You may be provided with forms to fill out that are similar, but not identical, to the sections on this page. Some of these examples reference documents in this "Public Servant Hackathon Guide" and will need to be included in the Appendix of any legal document.

# Evaluation Procedures

* Public servants must briefly contact past clients of vendors as part of the required evaluation
criteria for vendors.

# Statement of Work

### 4.0 Deliverables, Tasks and Technical Specifications

The contractor will perform all responsibilities for all [roles](../roles) defined by the Public Servant Hackathon Guide (see Appendix). This includes but is not limited to:

* Coordinating venue, food, and technology logistics.
* Performing as the master of ceremonies (MC).
* Sharing planning documents before the event begins, like a run sheet and announcement scripts.
* Planning and executing an effective judging process with prizes and rules.
* Marketing before and during the event.
* Participant recruitment efforts.
* Mentor recruitment efforts, and providing mentor orientation.
* Judge recruitment efforts, and providing judge orientation.
* Participating in a post-mortem process about the event.
* Provide representatives on site for all hours of the event.
