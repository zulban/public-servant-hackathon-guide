This page describes the orientation document that should be sent to presenters before the event starts.

# Basics

* List of contacts
* Where is the event?
* When is the event?
* Minimum expected time slots where the prenters must be present. If possible, only ask presenters to arrive for their presentation time and nothing more.
* How to drive to the venue, car parking description, or take public transit to the venue.

# Presenting

* Summary of [what a hackathon is](../overview.md).
* Explain how a hackthon is not a conference. Especially if there are monetary prizes, half the room might not be listening to a presentation. Let presenters know this is normal and it's okay.
