A runsheet is a chronological master list of all key events and times in the event. Every item on a runsheet must have a precise scheduled time, or time range.

This page is:

* a list of key events that should all be on any hackathon runsheet.
* a list of reminders, recommendations, and announcements made throughout the event

This page is not:

* a list of every obvious item on a real runsheet, like starting the award ceremony

# Announcements

* Announce when the submission deadline is 3 hours away, and 1 hour away.
* Announce 3 hours before the deadline that groups should stop working on their project, and start working on the submission. Especially in the case of a video or presentation, groups may wildly underestimate how long it may take to put one together. 
* Announce all live streams one hour before they start.

# Activity

* Once or twice a day, a mentor should tour all tables and ask every group if they need help.
* The last mentor to leave at night should tour all tables before leaving.

# Judging

* The first time the judges gather in one place, have them all briefy introduce themselves.

# Closing Ceremony

* Visually show the work of all winning teams.
* Welcome the winning team(s) on stage, if possible.
* Thank the organizers, participants, venue, everyone.
* Ask everyone to fill out the post-event survey.
* Provide the contact information of organisers.
* Provide a link that will communicate an event summary soon after the event.
