This page describes all the approvals that might be required for a government hackathon.

* Corporate Services and Finance Branch (CSFB)
* Shared Services Canada (SSC)
* Ethics Office
* Intellectual Property Office (IPO)
* Communications