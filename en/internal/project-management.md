This page describes the minimum requirements for a project management platform to plan a government hackathon.

# Access

* Private. Content is only visible to invited members.
* Accessible internally and externally. Anyone with internet access can be invited to join the platform.

# Features

At a minimum, a project management platform must have these features:

* List all tasks
* Tasks have: priorities, deadlines, assignees, discussions
* An overall view of all tasks and deadlines.
* Issues can: deadlines, priorities.
* Members can see different overviews of all issues, like a gantt chart or overdue issues.

# See Also

* [GitLab](https://gitlab.com)
