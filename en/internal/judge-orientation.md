This page describes the orientation document that should be sent to judges before the event starts.

# Basics

* List of contacts
* Where is the event?
* When is the event?
* Minimum expected time slots where the judges must be present.
* Short summary of the themes and challenges.
* List of prizes.
* How to drive to the venue, car parking description, or take public transit to the venue.

# Judging

* Summary of the judging process and schedule.
* Short description of what a submission will look like.
* Short description of the presentation or pitch format, if any.
* Judging criteria and any charts or rubriks used to judge groups.
