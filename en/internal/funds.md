This page lists helpful tips that may make it easier to manage funding and sponsors.

# Food

Different stakeholders or sponsors may have catering preferences. For example, Environment and Climate Change Canada may insist on sustainability.

# Prizes

It may be difficult for the government to explicitly hand out prizes. In the Values and Ethics Code for the Public Sector, public servants must not only be ethical, but also appear to be ethical. It looks bad to the public if there's a picture of a director or minister holding a giant check.

Here are some possible solutions:

* No cash prizes.
* Hire a private sector company to have complete control of the prizes.
* Alternative funding programs, like micro grants.

# Venue

Tech companies will often provide their venue free of charge for hackathons as a recruitment and marketing strategy. Consider finding a free venue from local sponsors.

# Sponsors

Funding may be spent more flexibility if it's obtained from external sponsors. However finding sponsors can take a minimum of 6 months.

Beware of conflicts of interest. Make sure the mission statement of the sponsors aligns with your [project charter](charter.md) and your department.
