This page lists prizes ideas and describes problems to avoid. See also: [funds](funds.md).

Not every prize needs a huge award. And not every prize needs to be awarded. However if a project deserves it, a special mention can still mean a lot to a hard working team.

# Special Prize Ideas

* Best visualization
* Most fun
* Best educational
* Best water-related
* Most surprising use of data.
* Best student submission
* Best public servant submission

# Open Source

Best open source solution can mean many things:

* the project code is open source but it uses closed source tools
* the project code is open source but it requires closed source operating systems
* the project code is open source
* the project code is FOSS and runs on fully FOSS operating systems

It's up to the organisers to decide how picky to be on this criteria.

At a minimum, this prize should require a standard `LICENSE` file found in any open source repository.
