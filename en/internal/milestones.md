This page describes the major milestones in planning a hackathon. Every item on this page must be done, and should be done in this order.

# 1. Project Charter

Write most of the short [project charter](charter.md) so everyone has a common understanding of the event.

# 2. Team

For each [role](../roles) defined in this guide, decide if it will be handled:

* externally by a private company
* internally by a public servant

For internal roles, find a specific public servant and confirm that they're interested. Add this information to the project charter.

# 3. Approval

Share the project charter with key sponsors and stakeholders, and get their approval.
