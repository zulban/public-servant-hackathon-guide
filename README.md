
# Public Servant Hackathon Guide

Ce guide aide les fonctionnaires à organiser des hackathons au gouvernement.

This guide helps public servants organize government hackathons.

## [English](en/readme.md)

## [Français](fr/readme.md)
